package ActionClass;

import PageObjectClass.HomePageObjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import util.Base;

import java.util.List;


public class HomePageAction extends Base {
    HomePageObjects ho = new HomePageObjects();


    public void searchContent(WebDriver driver, String contentText){
        waitForEleVisibility(driver,ho.searchIcon() , "xpath", 30, 500);
        click(driver, ho.searchIcon(),"xpath");
        waitForEleVisibility(driver, ho.searchTxtField(),"xpath", 8, 500);
        type(driver, ho.searchTxtField(), contentText);
        click(driver,ho.searchTxtField(),"xpath");
        waitForEleVisibility(driver,ho.searchResultsHeading(),"xpath", 10, 500 );
        int count = getEleList(driver,ho.searchResultsHeading(),"xpath").size();
        List<WebElement> eleList = getEleList(driver,ho.searchResultsHeading(),"xpath");
        for (int i=0;i<count;i++){
            String eleText = getEleText(driver,eleList.get(i),"xpath").toLowerCase();
            if(eleText.equals(contentText.trim().toLowerCase()){
                click(driver,eleList.get(i),"xpath");
                i = count;
            }
        }
    }
}
