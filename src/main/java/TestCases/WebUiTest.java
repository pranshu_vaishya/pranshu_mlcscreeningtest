package TestCases;

import ActionClass.HomePageAction;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import util.Base;
import util.Setup;
import util.TestData;

import java.lang.reflect.Method;



public class WebUiTest extends Base {
    WebDriver driver;
    SoftAssert sassert;

    HomePageAction ha = new HomePageAction();



    @BeforeMethod
    public void launchBrowser(Method method) throws Exception {
        print("Start of testcase : " + method.getName());
        Setup s = new Setup();
        driver = s.setbrowser(TestData.browser);
        s.selectWebsite(driver, TestData.mlcInsuranceURL);
        sassert = new SoftAssert();
    }

    @Test
    public void mlcFormFillUp_Part1(){
        ha.searchContent(driver, TestData.searchContent);
        //sysi
    }


    @AfterMethod
    public void tearDown(Method method) {
        driver.close();
        driver.quit();
        print("End of testcase : "+method.getName());
    }


}
