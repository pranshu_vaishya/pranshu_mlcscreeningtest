package PageObjectClass;

public class HomePageObjects {
    public String searchIcon(){
        return "//button[@aria-label='Toggle search']";
    }

    public String searchTxtField(){
        return "//input[@id='q']";
    }

    public String searchResultsHeading(){
        return "//div[@class='autocomplete-list']/ul/li/a/span";
    }
}
