package util;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

import java.util.concurrent.TimeUnit;

public class Setup extends Base{


        public WebDriver setbrowser(String browser) {

            switch (browser) {

                case "ie":
                    WebDriverManager.iedriver().setup();
                    return new InternetExplorerDriver();

                case "chrome":
                    WebDriverManager.chromedriver().setup();
                    return new ChromeDriver();

                default:
                    print("Please enter a valid browser name");

            }
            return null;
        }


        public void selectWebsite(WebDriver driver, String urlString) {
            driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
            driver.get(urlString);
            driver.manage().window().fullscreen();
        }


    }

