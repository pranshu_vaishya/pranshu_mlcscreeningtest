package util;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;
import java.time.Duration;
import java.util.List;
import java.util.NoSuchElementException;
import java.lang.String;

public class Base {

    public void waitForSpinner(WebDriver driver, String spinId, String spinEleType, int timeOut, int pollTime) {
            FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                    .withTimeout(Duration.ofSeconds(timeOut))
                    .pollingEvery(Duration.ofMillis(pollTime))
                    .ignoring(org.openqa.selenium.NoSuchElementException.class);
            switch(spinEleType){

                case "xpath":
                    try {
                        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(spinId)));
                        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(spinId)));
                    } catch (org.openqa.selenium.WebDriverException e1) {
                        print("Spinner exception caught !");
                    }
                    break;

                case "id":
                    try {
                        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(spinId)));
                        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id(spinId)));
                    } catch (org.openqa.selenium.WebDriverException e2) {
                        print("Spinner exception caught !");
                    }
                    break;

                default:
                    print("Invalid spinner identifier type argument passed ! ");
            }


        }


        public boolean valForElementVisibility(WebDriver driver, String eleId, String eleType, int timeOut, int pollTime) {
            FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                    .withTimeout(Duration.ofSeconds(timeOut))
                    .pollingEvery(Duration.ofMillis(pollTime));
            try {
                switch(eleType) {
                    case "xpath":
                        waitAndScrollToEle(driver, eleId,eleType,timeOut,pollTime);
                        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(eleId)));
                        break;

                    case "id":
                        waitAndScrollToEle(driver, eleId, eleType,timeOut,pollTime);
                        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(eleId)));
                        break;

                    default:
                        print("Invalid element type used for the element with identifier :"+eleId);
                }

            } catch (org.openqa.selenium.NoSuchElementException | org.openqa.selenium.TimeoutException e1) {
                return false;
            }
            return true;
        }


        /* ---------- Basic Web Actions ---------- */


        public void type(WebDriver driver, String eleIdentifier, String eleType, String value) {
            waitAndScrollToEle(driver,eleIdentifier,eleType , 5, 1000);
            switch(eleType) {
                case "xpath":
                    driver.findElement(By.xpath(eleIdentifier)).clear();
                    driver.findElement(By.xpath(eleIdentifier)).sendKeys(value);
                    driver.findElement(By.xpath(eleIdentifier)).sendKeys(Keys.TAB);
                    break;

                case "id":
                    driver.findElement(By.id(eleIdentifier)).clear();
                    driver.findElement(By.id(eleIdentifier)).sendKeys(value);
                    driver.findElement(By.id(eleIdentifier)).sendKeys(Keys.TAB);
                    break;

                default:
                    print("Invalid element type used for the element with identifier :"+eleIdentifier);
            }
        }

        public void type (WebDriver driver, String eleIdentifier, String value) {
            String eleType = "xpath";
            type(driver,eleIdentifier, eleType,value);
        }


        public void click(WebDriver driver, String eleIdentifier, String eleType){
            Actions action = new Actions(driver);
            switch(eleType) {
                case "xpath":
                    ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath(eleIdentifier)));
                    action.moveToElement(driver.findElement(By.xpath(eleIdentifier))).perform();
                    sleep(200);
                    action.moveToElement(driver.findElement(By.xpath(eleIdentifier))).click().perform();
                    break;

                case "id":
                    ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.id(eleIdentifier)));
                    action.moveToElement(driver.findElement(By.id(eleIdentifier))).perform();
                    sleep(200);
                    action.moveToElement(driver.findElement(By.id(eleIdentifier))).click().perform();
                    break;

                default:
                    print("Invalid element type used for the element with identifier :"+eleIdentifier);
            }
        }

        public void waitAndClick(WebDriver driver, String eleIdentifier, String eleType, int timeOut, int pollTime) {
            waitForEleVisibility(driver, eleIdentifier,eleType, timeOut, pollTime );
            click(driver, eleIdentifier, eleType);
        }

        public void waitAndClick(WebDriver driver, String eleIdentifier, int timeOut, int pollTime) {
            String eleType = "xpath";
            waitForEleVisibility(driver, eleIdentifier,eleType, timeOut, pollTime );
            click(driver, eleIdentifier, eleType);
        }


        public void waitForEleVisibility(WebDriver driver, String eleIdentifier, int timeOut, int pollTime) {
            String eleType = "xpath";
            waitForEleVisibility(driver, eleIdentifier, eleType,timeOut, pollTime);
        }

        public void waitForEleVisibility(WebDriver driver, String eleIdentifier, String eleType, int timeOut, int pollTime) {
            FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                    .withTimeout(Duration.ofSeconds(timeOut))
                    .pollingEvery(Duration.ofMillis(pollTime))
                    .ignoring(org.openqa.selenium.NoSuchElementException.class);
            switch(eleType) {
                case "xpath":
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(eleIdentifier)));
                    break;

                case "id":
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(eleIdentifier)));
                    break;

                default:
                    print("Invalid element type used for the element with identifier :"+eleIdentifier);
            }
        }

        public void waitForEleInvisibility(WebDriver driver, String eleIdentifier, String eleType, int timeOut, int pollTime) {
            FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                    .withTimeout(Duration.ofSeconds(timeOut))
                    .pollingEvery(Duration.ofMillis(pollTime))
                    .ignoring(org.openqa.selenium.NoSuchElementException.class);
            switch(eleType) {
                case "xpath":
                    wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(eleIdentifier)));
                    break;

                case "id":
                    wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id(eleIdentifier)));
                    break;

                default:
                    print("Invalid element type used for the element with identifier :"+eleIdentifier);
            }
        }

        public void selDropDwnVal(WebDriver driver, String eleIdentifier, String eleType, String value ){
            Select dropDown = null;
            waitAndScrollToEle(driver,eleIdentifier, eleType, 5, 1000);
            switch(eleType) {
                case "xpath":

                    dropDown = new Select(driver.findElement(By.xpath(eleIdentifier)));
                    break;

                case "id":
                    dropDown = new Select(driver.findElement(By.id(eleIdentifier)));
                    break;

                default:
                    print("Invalid element type used for the element with identifier :"+eleIdentifier);
            }
            dropDown.selectByVisibleText(value);
        }

        public String getDropDownVal (WebDriver driver, String eleIdentifier, String eleType) {
            Select dropDown = null;
            waitAndScrollToEle(driver,eleIdentifier, eleType, 5, 1000);
            switch(eleType) {
                case "xpath":

                    dropDown = new Select(driver.findElement(By.xpath(eleIdentifier)));
                    break;

                case "id":
                    dropDown = new Select(driver.findElement(By.id(eleIdentifier)));
                    break;

                default:
                    print("Invalid element type used for the element with identifier :"+eleIdentifier);
            }
            return dropDown.getFirstSelectedOption().getText().trim();

        }

        public void sleep(int millis){
            try {
                Thread.sleep(millis);
            } catch (InterruptedException exc) {
                exc.printStackTrace();
            }
        }

        public void switchToIframe(WebDriver driver, String iframeName, String nameType) {
            WebDriverWait wait = new WebDriverWait(driver, 25);
            switch(nameType){
                case "id":
                    try {
                        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(iframeName));
                        break;
                    } catch(org.openqa.selenium.NoSuchElementException | org.openqa.selenium.TimeoutException e1) {
                        print("Frame exception caught!");
                    }


                case "class":
                    try {
                        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className(iframeName)));
                        driver.switchTo().frame(driver.findElement(By.className(iframeName)));
                        break;
                    } catch(org.openqa.selenium.NoSuchElementException | org.openqa.selenium.TimeoutException e1) {
                        print("Frame exception caught !");
                    }


                default:
                    print("Invalid element type used for the iframe with Name :"+iframeName);
            }
        }

        public void waitAndScrollToEle(WebDriver driver, String eleId, int timeOut, int pollTime) {
            String eleType = "xpath";
            waitAndScrollToEle(driver, eleId, eleType,timeOut,pollTime);
        }

        public void waitAndScrollToEle(WebDriver driver, String eleId, String eleType, int timeOut, int pollTime) {
            FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                    .withTimeout(Duration.ofSeconds(timeOut))
                    .pollingEvery(Duration.ofMillis(pollTime))
                    .ignoring(NoSuchElementException.class);
            switch(eleType) {
                case "xpath":
                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(eleId)));
                    ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath(eleId)));
                    break;

                case "id":
                    wait.until(ExpectedConditions.presenceOfElementLocated(By.id(eleId)));
                    ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.id(eleId)));
                    break;

                default:
                    print("Invalid element type used for the element with identifier :"+eleId);

            }
        }



        public String getEleText (WebDriver driver, String eleId , String eleType) {
            String str = null;
            switch(eleType) {

                case "xpath":
                    str = driver.findElement(By.xpath(eleId)).getText();
                    break;

                case "id":
                    str = driver.findElement(By.id(eleId)).getText();
                    break;

                default:
                    print("Invalid element type used for the element with identifier :"+eleId);
            }
            return str;
        }

        public void pressKey(WebDriver driver,String xpath , String key ) {
            switch(key) {

                case "tab":
                    driver.findElement(By.xpath(xpath)).sendKeys(Keys.TAB);
                    break;

                case "enter":
                    driver.findElement(By.xpath(xpath)).sendKeys(Keys.ENTER);
                    break;

                default:
                    print("Invalid 'Key' type used with identifier :"+key);
            }

        }
    public void print(String text){
        System.out.println(text);
    }

    public List<WebElement> getEleList (WebDriver driver, String eleId, String eleType ){
        List<WebElement> eleList = null ;
        switch(eleType) {

            case "xpath":
                eleList = driver.findElements(By.xpath(eleId));
                break;

            case "id":
                eleList = driver.findElements(By.id(eleId));
                break;

            default:
                print("Invalid element type used for the element with identifier :"+eleId);
        }
        return eleList;


    }


}
